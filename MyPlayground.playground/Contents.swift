//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!

public enum DayOfWeek: Int, CustomStringConvertible {
	case Monday = 0, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
	
	public var description: String {
		return [
			"Каждый понедельник",
			"Каждый вторник",
			"Каждую среду",
			"Каждый четверг",
			"Каждую пятницу",
			"Каждую субботу",
			"Каждое воскресенье"][rawValue]
	}
	
	public var shortDescription:String { return ["Пн","Вт","Ср","Чт","Пт","Сб","Вс"][rawValue] }
	
	public func isDate(date:NSDate) -> Bool {
		let comps = calendar.components(.Weekday, fromDate: date)
		return comps.weekday == [2,3,4,5,6,7,1][rawValue]
	}
}

calendar.components([.Weekday, .Second], fromDate: NSDate())

struct Weekday : OptionSetType, CustomStringConvertible {
	let rawValue: Int
	
	init(rawValue: Int) { self.rawValue = rawValue }
	
	static let None = Weekday(rawValue: 0)
	static let Monday = Weekday(rawValue: 1 << 0)
	static let Tuesday = Weekday(rawValue: 1 << 1)
	static let Wednesday = Weekday(rawValue: 1 << 2)
	static let Thursday = Weekday(rawValue: 1 << 3)
	static let Friday = Weekday(rawValue: 1 << 4)
	static let Saturday = Weekday(rawValue: 1 << 5)
	static let Sunday = Weekday(rawValue: 1 << 6)
	static let Weekdays: Weekday = [.Monday, Tuesday, Wednesday, Thursday, Friday]
	static let Weekends: Weekday = [Saturday, Sunday]
	static let Week: Weekday = [Weekdays, Weekends]
	
	var description: String {
		switch self {
		case Weekday.None:
			return "Никогда"
		case Weekday.Monday:
			return "Каждый понедельник"
		case Weekday.Tuesday:
			return "Каждый вторниик"
		case Weekday.Wednesday:
			return "Каждую среду"
		case Weekday.Thursday:
			return "Каждый четверг"
		case Weekday.Friday:
			return "Каждую пятницу"
		case Weekday.Saturday:
			return "Каждую субботу"
		case Weekday.Sunday:
			return "Каждое воскресение"
		case Weekday.Weekdays:
			return "Будние дни"
		case Weekday.Weekends:
			return "Выходные"
		case Weekday.Week:
			return "Каждый день"
		default:
			let week: [(Weekday, String)] = [
				(.Monday, "Пн"),
				(.Tuesday, "Вт"),
				(.Wednesday, "Ср"),
				(.Thursday, "Чт"),
				(.Friday, "Пт"),
				(.Saturday, "Сб"),
				(.Sunday, "Вс")
			]
			return week.filter { self.contains($0.0) }.map { $0.1 }.joinWithSeparator(", ")
		}
	}
	
	func isDate(date:NSDate) -> Bool {
		let w = calendar.component(.Weekday, fromDate: date)
		let weekday = Weekday(rawValue: 1 << [5,6,0,1,2,3,4,5,6][w])
		return self.contains(weekday)
	}
}

var day: Weekday
day = [Weekday.Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday]

day.contains(.None)
day.contains(.Monday)
day.contains(.Tuesday)
day.contains(.Wednesday)
day.contains(.Thursday)
day.contains(.Friday)
day.contains(.Saturday)
day.contains(.Sunday)


for i in 0...6 {
	let date = calendar.dateByAddingUnit(.Day, value: i, toDate: NSDate(), options: NSCalendarOptions())!
	let w = calendar.component(.Weekday, fromDate: date)
	Weekday(rawValue: 1 << [5,6,0,1,2,3,4,5,6][w]).description
}









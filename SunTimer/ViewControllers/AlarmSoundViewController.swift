//
//  AlarmSoundViewController.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 08.04.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import UIKit

struct Sound {
	var name:String
	var fileName:String
	var length:Int
	
	static func SoundByFileName(fileName: String?) -> (index: Int, sound: Sound) {
		for (index, sound) in Sounds.enumerate() {
			if sound.fileName == fileName {
				return (index, sound)
			}
		}
		return (0, Sounds[0])
	}
	static let Sounds = [
		Sound(name: "По умолчанию", fileName: UILocalNotificationDefaultSoundName, length: 30),
		Sound(name: "Буль-буль", fileName: "ln.caf", length: 1),
		Sound(name: "Ситар", fileName: "sitar.caf", length: 27),
		Sound(name: "Пианино", fileName: "piano.caf", length: 29)
	];
}

class AlarmSoundViewController: UITableViewController {
	
	var alarm: Alarm!
	
	var selectedRow:Int = 0 {
		didSet {
			let old = NSIndexPath(forRow: oldValue, inSection: 0)
			let new = NSIndexPath(forRow: selectedRow, inSection: 0)
			tableView.reloadRowsAtIndexPaths([old, new], withRowAnimation: .Automatic)
			
			let sound = Sound.Sounds[selectedRow]
			alarm.sound = sound.fileName
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		selectedRow = Sound.SoundByFileName(alarm.sound).index
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Sound.Sounds.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("soundCell", forIndexPath: indexPath)

		let sound = Sound.Sounds[indexPath.row]
		
        cell.textLabel?.text = sound.name
		cell.detailTextLabel?.text = "\(sound.length) сек."
		cell.accessoryType = selectedRow == indexPath.row ? .Checkmark : .None

        return cell
    }
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		selectedRow = indexPath.row
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

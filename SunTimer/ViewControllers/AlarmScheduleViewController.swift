//
//  AlarmScheduleViewController.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 27.09.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import UIKit

class AlarmScheduleViewController: UITableViewController {
	
	var alarm: Alarm!
	
	let week: [Weekday] = [.Monday, .Tuesday, .Wednesday, .Thursday, .Friday, .Saturday, .Sunday]

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
		
		let day = week[indexPath.row]
		cell.textLabel?.text = day.description
		cell.accessoryType = alarm.schedule.contains(day) ? .Checkmark : .None

        return cell
    }

	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let day = week[indexPath.row]
		if alarm.schedule.contains(day) {
			alarm.schedule.remove(day)
		} else {
			alarm.schedule.insert(day)
		}
		tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
	}
}

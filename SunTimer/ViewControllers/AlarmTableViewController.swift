//
//  AlarmTableViewController.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 19.09.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import UIKit
import CoreData

class AlarmTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
	
	var frc = NSFetchedResultsController()
	var updatableAlarmVCs = [UpdatableAlarmVC]()

    override func viewDidLoad() {
        super.viewDidLoad()
		
		let sortByTime = NSSortDescriptor(key: "timeInMinutes", ascending: true)
		let sortByName = NSSortDescriptor(key: "name", ascending: true)
		let request = NSFetchRequest(entityName: "Alarm")
		request.sortDescriptors = [sortByTime, sortByName]
		frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
		frc.delegate = self
		do {
			try frc.performFetch()
		} catch {
			fatalError("Failed to initialize FetchedResultsController: \(error)")
		}
		
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
    }
	
	override func viewDidAppear(animated: Bool) {
		updatableAlarmVCs.removeAll()
	}

	// MARK: - Controller context chenging
	
	func controllerWillChangeContent(controller: NSFetchedResultsController) {
		tableView.beginUpdates()
	}
	
	func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
		switch type {
		case .Insert:
			tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
		case .Delete:
			tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
		case .Update:
			tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
		case .Move:
			tableView.moveRowAtIndexPath(indexPath!, toIndexPath: newIndexPath!)
			tableView.endUpdates()
			tableView.beginUpdates()
			tableView.reloadRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
		}
		updatableAlarmVCs.forEach { if $0.alarm === anObject { $0.update() } }
	}
	
	func controllerDidChangeContent(controller: NSFetchedResultsController) {
		tableView.endUpdates()
	}
	
    // MARK: - Table view data source
	
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return frc.sections!.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frc.sections![section].numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! AlarmTableCell

        let alarm = frc.objectAtIndexPath(indexPath) as! Alarm
		cell.alarm = alarm

        return cell
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
			let alarm = frc.objectAtIndexPath(indexPath) as! Alarm
			context.deleteObject(alarm)
			save()
        }
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		let alarmVC = segue.destinationViewController as! AlarmViewController
		if segue.identifier == "edit" {
			let indexPath = tableView.indexPathForSelectedRow!
			let alarm = frc.objectAtIndexPath(indexPath) as! Alarm
			alarmVC.alarm = alarm
		} else if segue.identifier == "add" {
			alarmVC.alarm = NSEntityDescription.insertNewObjectForEntityForName("Alarm", inManagedObjectContext: context) as? Alarm
		}
		if let updVC = segue.destinationViewController as? UpdatableAlarmVC {
			updatableAlarmVCs.append(updVC)
		}
    }
	
	@IBAction func unwindFromChildView(segue: UIStoryboardSegue) {}
}


// MARK: - Updatable protocol

protocol UpdatableAlarmVC {
	var alarm: Alarm! { get set }
	func update()
}


// MARK: - AlarmsTableCell class

class AlarmTableCell: UITableViewCell {
	
	var alarm: Alarm? = nil {
		didSet {
			if let alarm = alarm {
				let date = NSDate().dateFromTimeInMinutes(Int(alarm.time))
				timeLabel.text = (alarm.isSunTime ? "☀️ " : "🌎 ") + timeFormatter.stringFromDate(date)
				
				let name = alarm.name + (alarm.schedule == .None ? "" : ", " + alarm.schedule.description)
				let attrText = NSMutableAttributedString(string: name, attributes: nil)
				let range:NSRange = NSMakeRange(0, (alarm.name as NSString).length)
				attrText.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(16), range: range)
				nameLabel.attributedText = attrText
				
				onSwitch.on = alarm.isOn
				backgroundColor = onSwitch.on ? UIColor.whiteColor() : UIView().backgroundColor
				timeLabel.alpha = onSwitch.on ? 1.0 : 0.5
				nameLabel.alpha = onSwitch.on ? 1.0 : 0.5
			}
		}
	}
	
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var onSwitch: UISwitch!
	
	@IBAction func switchOn() {
		if let alarm = alarm {
			alarm.isOn = onSwitch.on
			save()
		}
	}
}

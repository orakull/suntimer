//
//  AlarmPropertiesTableViewController.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 20.09.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import UIKit
import CoreData

extension NSDate {
	var timeInMinutes: Int {
		let comps = calendar.components([.Hour, .Minute], fromDate: self)
		return comps.hour * 60 + comps.minute
	}
	func dateFromTimeInMinutes(time: Int) -> NSDate {
		return calendar.dateBySettingHour(time / 60, minute: time % 60, second: 0, ofDate: self, options: NSCalendarOptions())!
	}
}

extension UIView {
	func runTransition(duration:CFTimeInterval, type:String = kCATransitionFade) {
		let animation:CATransition = CATransition()
		animation.timingFunction = CAMediaTimingFunction(name:
			kCAMediaTimingFunctionEaseInEaseOut)
		animation.type = type
		animation.duration = duration
		self.layer.addAnimation(animation, forKey: nil)
	}
}

class AlarmViewController: UITableViewController, UpdatableAlarmVC {
	
	var alarm: Alarm!
	
	@IBOutlet weak var datePicker: UIDatePicker!
	@IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var soundLabel: UILabel!
	@IBOutlet weak var isRepeatSwitch: UISwitch!
	@IBOutlet weak var isSunTimeSwitch: UISwitch!
	@IBOutlet weak var icon: UILabel!
	
	@IBAction func saveTapped(_: AnyObject) {
		save()
		navigationController?.popViewControllerAnimated(true)
	}
	
	@IBAction func cancelTapped(_: AnyObject) {
		context.rollback()
		navigationController?.popViewControllerAnimated(true)
	}
	
	@IBAction func timeChanged() {
		alarm.time = datePicker.date.timeInMinutes
		updateTitle()
	}
	@IBAction func isRepeatChanged() {
		alarm.isRepeat = isRepeatSwitch.on
	}
	@IBAction func isSunTimeChanged() {
		alarm.isSunTime = isSunTimeSwitch.on
		datePicker.setDate(
			alarm.isSunTime
				? sunTime.sunDateFromDate(datePicker.date)
				: sunTime.sunDateToDate(datePicker.date),
			animated: true)
		alarm.time = datePicker.date.timeInMinutes
		updateTitle()
	}
    override func viewDidLoad() {
		update()
		updateTitle()
    }
	
	func update() {
		datePicker?.date = NSDate().dateFromTimeInMinutes(Int(alarm.time))
		scheduleLabel?.text = alarm.schedule.description
		nameLabel?.text = alarm.name
		soundLabel?.text = Sound.SoundByFileName(alarm.sound).sound.name
		isRepeatSwitch?.on = alarm.isRepeat
		isSunTimeSwitch?.on = alarm.isSunTime
	}
	
	func updateTitle() {
		if (isSunTimeSwitch.on) {
			title = "🌎 " + timeFormatter.stringFromDate(sunTime.sunDateToDate(datePicker.date))
			if (icon.text != "☀️") {
				icon.runTransition(0.2, type: kCATransitionMoveIn)
				icon.text = "☀️"
			}
		} else {
			title = "☀️ " + timeFormatter.stringFromDate(sunTime.sunDateFromDate(datePicker.date))
			if (icon.text != "🌎")
			{
				icon.runTransition(0.2, type: kCATransitionMoveIn)
				icon.text = "🌎"
			}
		}
	}
	
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return alarm.objectID.temporaryID ? 1 : 2
    }
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let cell = tableView.cellForRowAtIndexPath(indexPath)!
		if cell.reuseIdentifier == "deleteCell" {
			performSegueWithIdentifier("backToAlarmTable", sender: self)
			context.deleteObject(alarm)
			save()
		}
	}
	
	override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		if let cell = tableView.cellForRowAtIndexPath(indexPath) {
			if ["time", "sunTime", "repeat"].contains(cell.reuseIdentifier!) {
				return false
			}
		}
		return true
	}

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if let nameVC = segue.destinationViewController as? AlarmNameViewController {
			nameVC.alarm = alarm
		}
		if let scheduleVC = segue.destinationViewController as? AlarmScheduleViewController {
			scheduleVC.alarm = alarm
		}
		if let soundVC = segue.destinationViewController as? AlarmSoundViewController {
			soundVC.alarm = alarm
		}
    }
	
	@IBAction func unwindFromChildView(segue: UIStoryboardSegue) {}
	
}

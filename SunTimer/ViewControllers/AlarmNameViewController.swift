//
//  AlarmNameViewController.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 21.09.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import UIKit

class AlarmNameViewController: UITableViewController, UITextFieldDelegate {

	var alarm: Alarm!
	
	@IBOutlet weak var nameTextField: UITextField!
	
	@IBAction func nameChanged() {
		alarm.name = nameTextField.text ?? ""
	}
	
	override func viewDidLoad() {
		nameTextField.text = alarm.name
		nameTextField.delegate = self
	}
	
	override func viewDidAppear(animated: Bool) {
		nameTextField.becomeFirstResponder()
	}
	
	override func viewWillDisappear(animated: Bool) {
		if alarm.name == "" {
			alarm.name = "Будильник"
		}
	}
	
    // MARK: - Table view data source
	
	override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return false
	}
	
	// MARK: - TextFieldDelegate
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		performSegueWithIdentifier("backToAlarm", sender: self)
		return true
	}
}

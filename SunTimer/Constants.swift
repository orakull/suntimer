//
//  Constants.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 31.05.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//

import Foundation

struct Constants {
	static let CATEGORY_ALERT = "CATEGORY_ALERT"
	static let CATEGORY_REPEAT = "CATEGORY_REPEAT"
	static let ACTION_READY = "ACTION_READY"
	static let ACTION_LATER = "ACTION_LATER"
}
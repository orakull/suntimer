//
//  Settings.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 17.10.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import Foundation

class Settings {
	
	private let plist: Plist
	private let url: NSURL
	
	init () {
		let documentDir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
		let path = NSURL(string: documentDir)!.URLByAppendingPathComponent("settings.plist").absoluteString
		if !NSFileManager.defaultManager().fileExistsAtPath(path) {
			let pathInBundle = NSBundle.mainBundle().pathForResource("settings", ofType: "plist")!
			do {
				try NSFileManager.defaultManager().copyItemAtPath(pathInBundle, toPath: path)
				print("settings.plist copied")
			} catch {
				print("error coping plist!")
			}
		} else {
			print("plist exists")
		}
		url = NSURL(fileURLWithPath: path)
		plist = try! Plist.propertyListWithContentsOfURL(url)!
	}
	
	func getParams() -> Params {
		let latitude = plist[ParamsName.latitude]!.double!
		let longitude = plist[ParamsName.longitude]!.double!
		let date = plist[ParamsName.date]!.date!
		let localOffset = plist[ParamsName.localOffset]!.double!
		
		return Params(latitude: latitude, longitude: longitude, date: date, localOffset: localOffset)
	}
	
	func setParams(params: Params) {
		plist[ParamsName.latitude] = Plist(number: params.latitude)
		plist[ParamsName.longitude] = Plist(number: params.longitude)
		plist[ParamsName.date] = Plist(date: params.date)
		plist[ParamsName.localOffset] = Plist(number: params.localOffset)
		
		try! Plist.writePropertyListToURL(plist, url: url)
	}
}

private struct ParamsName {
	static let latitude = "latitude"
	static let longitude = "longitude"
	static let date = "date"
	static let localOffset = "localOffset"
}
	
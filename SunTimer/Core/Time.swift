//
//  Scheduler.swift
//  SunTimeKit
//
//  Created by Руслан Ольховка on 13.07.15.
//  Copyright (c) 2015 Руслан Ольховка. All rights reserved.
//

import Foundation

struct Weekday : OptionSetType, CustomStringConvertible {
	let rawValue: Int
	
	init(rawValue: Int) { self.rawValue = rawValue }
	
	static let None = Weekday(rawValue: 0)
	static let Monday = Weekday(rawValue: 1 << 0)
	static let Tuesday = Weekday(rawValue: 1 << 1)
	static let Wednesday = Weekday(rawValue: 1 << 2)
	static let Thursday = Weekday(rawValue: 1 << 3)
	static let Friday = Weekday(rawValue: 1 << 4)
	static let Saturday = Weekday(rawValue: 1 << 5)
	static let Sunday = Weekday(rawValue: 1 << 6)
	static let Weekdays: Weekday = [.Monday, Tuesday, Wednesday, Thursday, Friday]
	static let Weekends: Weekday = [Saturday, Sunday]
	static let Week: Weekday = [Weekdays, Weekends]
	
	var description: String {
		switch self {
		case Weekday.None:
			return "Никогда"
		case Weekday.Monday:
			return "Понедельник"
		case Weekday.Tuesday:
			return "Вторниик"
		case Weekday.Wednesday:
			return "Среда"
		case Weekday.Thursday:
			return "Четверг"
		case Weekday.Friday:
			return "Пятница"
		case Weekday.Saturday:
			return "Суббота"
		case Weekday.Sunday:
			return "Воскресенье"
		case Weekday.Weekdays:
			return "Будние дни"
		case Weekday.Weekends:
			return "Выходные"
		case Weekday.Week:
			return "Каждый день"
		default:
			let week: [(Weekday, String)] = [
				(.Monday, "Пн"),
				(.Tuesday, "Вт"),
				(.Wednesday, "Ср"),
				(.Thursday, "Чт"),
				(.Friday, "Пт"),
				(.Saturday, "Сб"),
				(.Sunday, "Вс")
			]
			return week.filter { self.contains($0.0) }.map { $0.1 }.joinWithSeparator(", ")
		}
	}
	
	func isDate(date:NSDate) -> Bool {
		let w = calendar.component(.Weekday, fromDate: date)
		let weekday = Weekday(rawValue: 1 << [5,6,0,1,2,3,4,5,6][w])
		return self.contains(weekday)
	}
}

public struct Time {
	public var rawMinutes = 0
	public var minutes: Int { return rawMinutes % 60 }
	public var hours: Int { return rawMinutes / 60 }
	
	public init() {
		self.init(date: NSDate().dateByAddingTimeInterval(60))
	}
	public init(rawMinutes: Int) {
		self.rawMinutes = rawMinutes
	}
	public init(date: NSDate) {
		let comps = calendar.components([.Hour, .Minute], fromDate: date)
		self.init(hour: comps.hour, minute: comps.minute)
	}
	public init(hour: Int, minute: Int) {
		self.rawMinutes = hour * 60 + minute
	}
	
	/// near Date in future for the Time
	public var nearDate: NSDate {
		let comps = calendar.components([.Era, .Year, .Month, .Day], fromDate: NSDate())
		comps.hour = hours
		comps.minute = minutes
		let date = calendar.dateFromComponents(comps)!
		return date.nearest
	}
}

let calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!

private var tf: NSDateFormatter?
var timeFormatter: NSDateFormatter {
	if tf == nil {
		tf = NSDateFormatter()
		tf!.timeStyle = .ShortStyle
		tf!.dateStyle = .NoStyle
	}
	return tf!
}

extension NSDate {
	var weekday: Weekday {
		let w = calendar.component(.Weekday, fromDate: self)
		return Weekday(rawValue: 1 << [5,6,0,1,2,3,4,5,6][w])
	}
}


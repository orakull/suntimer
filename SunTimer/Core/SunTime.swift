//
//  SunTime.swift
//  SunTimeKit
//
//  Created by Руслан Ольховка on 02.07.15.
//  Copyright (c) 2015 Руслан Ольховка. All rights reserved.
//

import Foundation

extension Double {
	var rad: Double { return self * M_PI / 180	}
	var deg: Double { return self * 180 / M_PI }
	var round: Double { return (self + 360) % 360 }
}

extension NSDate {
	
	var nearest: NSDate { return nearestFor(NSDate()) }
	
	func nearestFor(date: NSDate) -> NSDate {
		var nearDate = self
		while calendar.compareDate(nearDate, toDate: date, toUnitGranularity: .Second) == .OrderedDescending {
			nearDate = calendar.dateByAddingUnit(.Day, value: -1, toDate: nearDate, options: NSCalendarOptions())!
		}
		while calendar.compareDate(nearDate, toDate: date, toUnitGranularity: .Second) == .OrderedAscending {
			nearDate = calendar.dateByAddingUnit(.Day, value: 1, toDate: nearDate, options: NSCalendarOptions())!
		}
		return nearDate
	}
	
	static func dateFrom(year year:Int, month: Int, day: Int, hour: Int, minute: Int, second: Int) -> NSDate? {
		let comps = calendar.components([.Era, .TimeZone, .Calendar], fromDate: NSDate())
		comps.year = year
		comps.month = month
		comps.day = day
		comps.hour = hour
		comps.minute = minute
		comps.second = second
		return calendar.dateFromComponents(comps)
	}
}

struct Params {
	var latitude: Double
	var longitude: Double
	var date: NSDate
	var localOffset: Double
	
	init (latitude: Double, longitude: Double, date: NSDate, localOffset: Double = 0.0) {
		self.latitude = latitude
		self.longitude = longitude
		self.date = date
		self.localOffset = localOffset ?? Double(calendar.components(.TimeZone, fromDate: date).timeZone!.secondsFromGMT) / 3600
	}
}

class SunTime
{
	var params: Params
	
	let zenithDefault: Double = 90 + 50/60
	var zenithIdeal: Double = 0
	
	init(_ params: Params) {
		self.params = params
		self.sunrise = calculate(true, zenith: zenithDefault)
		self.sunset = calculate(false, zenith: zenithDefault)
		
		let sr = calculate(true, zenith: zenithIdeal)!
		let ss = calculate(false, zenith: zenithIdeal)!
		diff = calendar.components([.Hour, .Minute, .Second], fromDate: ss, toDate: sr, options: NSCalendarOptions())
		let diffInSec = diff.second + 60 * (diff.minute + 60 * diff.hour)
		if diffInSec > 0 {
			self.midnight = calendar.dateByAddingUnit(.Second, value: diffInSec/2, toDate: ss, options: NSCalendarOptions())!.nearestFor(params.date)
			self.noon = calendar.dateByAddingUnit(.Hour, value: 12, toDate: midnight, options: NSCalendarOptions())!.nearestFor(params.date)
		} else {
			self.noon = calendar.dateByAddingUnit(.Second, value: diffInSec/2, toDate: ss, options: NSCalendarOptions())!.nearestFor(params.date)
			self.midnight = calendar.dateByAddingUnit(.Hour, value: 12, toDate: noon, options: NSCalendarOptions())!.nearestFor(params.date)
		}
		
		self.diff = calendar.components([.Hour, .Minute, .Second], fromDate: midnight)
	}
	
	var sunrise: NSDate?
	var sunset: NSDate?
	var noon: NSDate = NSDate()
	var midnight: NSDate = NSDate()
	var diff:NSDateComponents = NSDateComponents()
	var diffInSec:Int {
		return diff.second + 60 * (diff.minute + 60 * diff.hour)
	}
	
	func sunDateFromDate(date:NSDate) -> NSDate {
		var params = self.params
		params.date = date
		let st = SunTime(params)
		return calendar.dateByAddingUnit(.Second, value: -st.diffInSec, toDate: date, options: NSCalendarOptions())!
	}
	
	func sunDateToDate(date:NSDate) -> NSDate {
		var params = self.params
		params.date = date
		let st = SunTime(params)
		return calendar.dateByAddingUnit(.Second, value: st.diffInSec, toDate: date, options: NSCalendarOptions())!
	}
	
	func calculate(rising: Bool, zenith: Double) -> NSDate? {
		// 1. first calculate the day of the year
		let dateFormatter = NSDateFormatter()
		dateFormatter.dateFormat = "D"
		let N = Double(Int(dateFormatter.stringFromDate(params.date))!)
		
		// 2. convert the longitude to hour value and calculate an approximate time
		let lngHour = params.longitude / 15
		let t = N + ((rising ? 6 : 18) - lngHour) / 24
		
		// 3. calculate the Sun's mean anomaly
		let M = 0.9856 * t - 3.289
		
		// 4. calculate the Sun's true longitude
		let L = (M + 1.916 * sin(M.rad) + 0.02 * sin(2 * M.rad) + 282.634).round
		
		// 5a. calculate the Sun's right ascension
		var RA = atan(0.91764 * tan(L.rad)).deg.round
		
		// 5b. right ascension value needs to be in the same quadrant as L
		let Lquadrant = floor(L / 90) * 90
		let RAquadrant = floor(RA / 90) * 90
		RA += Lquadrant - RAquadrant
		
		// 5c. right ascension value needs to be converted into hours
		RA /= 15
		
		// 6. calculate the Sun's declination
		let sinDec = 0.39782 * sin(L.rad)
		let cosDec = cos(asin(sinDec))
		
		// 7a. calculate the Sun's local hour angle
		let cosH = (cos(zenith.rad) - sinDec * sin(params.latitude.rad)) / (cosDec * cos(params.latitude.rad))
		self.zenithIdeal = acos(sinDec * sin(params.latitude.rad)).deg
		if cosH > 1 {
			print("the sun never rises on this location (on the specified date)")
			return nil
		} else if cosH < -1 {
			print("the sun never sets on this location (on the specified date)")
			return nil
		}
		
		// 7b. finish calculating H and convert into hours
		let H = (rising ? 360 - acos(cosH).deg : acos(cosH).deg) / 15
		
		// 8. calculate local mean time of rising/setting
		let T = H + RA - 0.06571 * t - 6.622
		
		// 9. adjust back to UTC
		let UT = T - lngHour
		
		// 10. convert UT value to local time zone of latitude/longitude
		let localT = UT + params.localOffset
		
		// 11. convert to NSDate in the near future
		let comps = calendar.components([.Era, .Year, .Month, .Day, .TimeZone], fromDate: params.date)
		var time = Int(localT * 60 * 60)
		comps.second = time % 60
		time /= 60
		comps.minute = time % 60
		time /= 60
		comps.hour = time % 24
		return calendar.dateFromComponents(comps)!.nearestFor(params.date)
	}
}
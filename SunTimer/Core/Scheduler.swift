//
//  Scheduler.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 29.09.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Scheduler {
	
	static func scheduleAll() {
		UIApplication.sharedApplication().cancelAllLocalNotifications()
		print("scheduleAll")
		let request = NSFetchRequest(entityName: "Alarm")
		var alarms = try! context.executeFetchRequest(request) as! [Alarm]
		
		var date = NSDate()
		var count = 0
		while count < 64 && alarms.count > 0 {
			for alarm in alarms {
				alarm.checkDeactivation()
				if alarm.isOn {
					if alarm.schedule == .None {
						if scheduleAlarm(alarm, date: date) {
							let index = alarms.indexOf(alarm)!
							alarms.removeAtIndex(index)
							count += alarm.isRepeat ? 5 : 1;
						}
					} else if alarm.schedule.isDate(date) {
						if scheduleAlarm(alarm, date: date) {
							if !alarm.isSunTime {
								alarm.schedule.subtractInPlace(date.weekday)
								if alarm.schedule == .None {
									let index = alarms.indexOf(alarm)!
									alarms.removeAtIndex(index)
								}
							}
							count += alarm.isRepeat ? 5 : 1;
						}
					}
				} else {
					let index = alarms.indexOf(alarm)!
					alarms.removeAtIndex(index)
				}
			}
			
			date = date.dateByAddingTimeInterval(60 * 60 * 24)
		}
		context.rollback()
	}
	
	private static func scheduleAlarm(alarm: Alarm, date:NSDate) -> Bool {
		var fireDate = date.dateFromTimeInMinutes(alarm.time)
		if alarm.isSunTime {
			fireDate = sunTime.sunDateToDate(fireDate)
			fireDate = date.dateFromTimeInMinutes(fireDate.timeInMinutes)
		}
		if fireDate.timeIntervalSince1970 <= NSDate().timeIntervalSince1970 {
			return false
		}
		
		let notif = UILocalNotification()
		notif.fireDate = fireDate
		print(alarm.name, fireDate)
		if alarm.schedule != .None && !alarm.isSunTime {
			notif.repeatInterval = .Weekday
			print("repeat every", date.weekday.description)
		}
		if alarm.schedule == .None {
			alarm.deactivateDate = fireDate.timeIntervalSince1970
			save(false)
		} else {
			alarm.deactivateDate = 0
		}
		
		notif.category = alarm.isRepeat ? Constants.CATEGORY_REPEAT : Constants.CATEGORY_ALERT
		
		notif.alertBody = (alarm.isSunTime ? "☀️" : "🌎")
			+ " \(timeFormatter.stringFromDate(fireDate)) " + alarm.name
		notif.soundName = alarm.sound
		UIApplication.sharedApplication().scheduleLocalNotification(notif)
		
		if alarm.isRepeat {
			for i in 1..<5 {
				let notifR = UILocalNotification()
				notifR.alertBody = notif.alertBody
				notifR.alertTitle = notif.alertTitle
				notifR.soundName = notif.soundName
				notifR.repeatInterval = notif.repeatInterval
				notifR.category = Constants.CATEGORY_REPEAT
				
				let sound = Sound.SoundByFileName(alarm.sound).sound
				notifR.fireDate = fireDate.dateByAddingTimeInterval(NSTimeInterval(i * sound.length))
				UIApplication.sharedApplication().scheduleLocalNotification(notifR)
				print(alarm.name, notifR.fireDate!)
			}
		}
		
		return true
	}

}
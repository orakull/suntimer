//
//  Alarm.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 19.09.15.
//  Copyright © 2015 Руслан Ольховка. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Alarm: NSManagedObject {
	
	var schedule: Weekday {
		get {
			return Weekday(rawValue: Int(scheduleCoded))
		}
		set {
			scheduleCoded = Int16(newValue.rawValue)
		}
	}
	
	var time: Int {
		get {
			return Int(timeInMinutes)
		}
		set {
			timeInMinutes = Int16(newValue)
		}
	}
	
	override func awakeFromInsert() {
		time = NSDate().timeInMinutes
		sound = UILocalNotificationDefaultSoundName
	}
	
	func checkDeactivation() {
		if deactivateDate > 0 {
			let now = NSDate().timeIntervalSince1970
			if deactivateDate <= now {
				isOn = false
				deactivateDate = 0
				save()
			}
		}
	}
}

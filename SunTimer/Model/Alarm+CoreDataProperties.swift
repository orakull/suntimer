//
//  Alarm+CoreDataProperties.swift
//  SunTimer
//
//  Created by Руслан Ольховка on 10.04.16.
//  Copyright © 2016 Руслан Ольховка. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Alarm {

    @NSManaged var deactivateDate: NSTimeInterval
    @NSManaged var isOn: Bool
    @NSManaged var isRepeat: Bool
    @NSManaged var isSunTime: Bool
    @NSManaged var name: String
    @NSManaged var scheduleCoded: Int16
    @NSManaged var timeInMinutes: Int16
    @NSManaged var sound: String

}
